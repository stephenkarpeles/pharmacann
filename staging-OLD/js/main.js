/* ===========================================================
   jQuery Smooth Scroll
   =========================================================== */

$(function() {
  $('a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 800);
        return false;
      }
    }
  });
});

/* ===========================================================
   Swap Assets Image
   =========================================================== */

$(document).ready(function($) {
    $(".state-choice-list .1-option-label").click(function() {
      $('.map-1').fadeIn(600);
      $('.state-content-1').fadeIn(600);
      $('.map-2').hide();
      $('.state-content-2').hide();
    });
});

$(document).ready(function($) {
    $(".state-choice-list .2-option-label").click(function() {
      $('.map-2').fadeIn(600);
      $('.state-content-2').fadeIn(600);
      $('.map-1').hide();
      $('.state-content-1').hide();
    });
});

/* ===========================================================
   Mobile Menu
   =========================================================== */

$(document).ready(function($) {
    $("#menuToggle").click(function() {
      $('header .mobile-nav-wrap nav').toggleClass('visible');
      $('header .mobile-nav-wrap nav').toggleClass('height-auto');
      $('header').toggleClass('mobile-menu-height-open');
    });
});

$(document).ready(function($) {
    $("header .mobile-nav-wrap nav a").click(function() {
      $('header').removeClass('mobile-menu-height-open');
      $('header .mobile-nav-wrap nav').toggleClass('visible');
    });
});

/*$(document).on('click', function(event) {
  if (!$(event.target).closest('header').length) {
    $('header .mobile-nav-wrap nav').hide();
    $('header').removeClass('mobile-menu-height-open');
  }
});
*/

/* =====================================================
   Show Top Link After Scrolling
   ===================================================== */

$(window).scroll(function() {
    if ($(this).scrollTop() > 505) { //use `this`, not `document`
        $('a.top-link').css({
            'display': 'block'
        });
    }
});

$(window).scroll(function() {
    if ($(this).scrollTop() < 504) { //use `this`, not `document`
        $('a.top-link').css({
            'display': 'none'
        });
    }
});

/* =====================================================
   Sticky Header
   ===================================================== */

$(window).scroll(function() {
    if ($(this).scrollTop() > 105) { //use `this`, not `document`
        $('header').css({
            'background': 'rgba(0, 0, 0, .4)'
        });
        $('.logo').css({
            'top': '15px'
        });
        $('header .nav-wrap').css({
            'top': '25px'
        });
        $('header .mobile-nav-wrap').css({
            'top': '25px'
        });
    }
});

$(window).scroll(function() {
    if ($(this).scrollTop() < 104) { //use `this`, not `document`
        $('header').css({
            'background': 'transparent'
        });
        $('.logo').css({
            'top': '60px'
        });
        $('header .nav-wrap').css({
            'top': '75px'
        });
        $('header .mobile-nav-wrap').css({
            'top': '75px'
        });
        $('.legal-page header .mobile-nav-wrap').css({
            'top': '35px'
        });
        $('.legal-page header').css({
            'background': '#444'
        });
    }
});

/* =====================================================
   Hero Image Fill Fallback
   ===================================================== */

$(function () {
  if ( ! Modernizr.objectfit ) {
    $('.hero .fit-img').each(function () {
      var $container = $(this),
          imgUrl = $container.find('img').prop('src');
      if (imgUrl) {
        $container
          .css('backgroundImage', 'url(' + imgUrl + ')')
          .addClass('compat-object-fit');
      }  
    });
  }
});